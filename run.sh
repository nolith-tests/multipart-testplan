#!/bin/sh


mkdir -p lfs
TEMPFILE=$(mktemp lfs/XXXXX) || exit 1
echo "I'm an LFS object!\n$TEMPFILE" > $TEMPFILE
git checkout -b $TEMPFILE
git add lfs/*
git commit -m "Automated test - $TEMPFILE"
git push --set-upstream origin $TEMPFILE
git checkout master
