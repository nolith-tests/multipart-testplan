# MultipartUpload Test Plan

This repository containes a seed project for testing [Multipart Upload support in workhorse](https://gitlab.com/gitlab-org/gitlab-ce/issues/44142)

Unfortunately this test involves some manual steps and this project is here to help a bit.

## Setup

1. Prepare a deployment with the proper versions of workhorse and gitlab-rails
1. Configure a runner
1. Create a new project importing this repository into the test environment

### Runner configuration

A working docker executor will be ok, if using a shell runner on macOS please make sure you have `coreutils` installed.

## Configurations

We need to test different provider and `direct_upload` enabled and disabled.

Snippet of relevant gitlab.yml` parts:

```yml
    artifacts:
        enabled: true
        object_store: 
        enabled: $os_enabled
        direct_upload: $du_enabled
        remote_directory: artifacts
        connection:
            provider: $provider
            # credentials based on provider
    lfs:
        enabled: true
        object_store:
        enabled: $os_enabled
        direct_upload: $du_enabled
        remote_directory: lfs-objects
        connection:
            provider: $provider
            # credentials based on provider
```

| Scenario | `$os_enabled` | `$du_enabled` | `$provider` | notes |
|----------|-------------|-------------|-----------|-------|
| Local storage | false | false | - | `connection` here is not relevant |
| GCS - direct | true | true | Google | | 
| GCS - sidekiq | true | false | Google | | 
| AWS - direct | true | true | AWS | | 
| AWS - sidekiq | true | false | AWS | | 
| minio - direct | true | true | AWS | [howto setup in GDK](https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/doc/howto/object_storage.md) | 
| minio - sidekiq | true | false | AWS | Not supported directly by GDK, but you can change the value manually |

## Running the test

1. clone the project
    
    ```shell
    cd /tmp
    git clone http://mygitlab/myuser/test.git
    cd test
    ```
1. run the [test script](https://gitlab.com/nolith-tests/multipart-testplan/blob/master/run.sh) 
    
    ```shell
    $ ./run.sh
    Switched to a new branch 'lfs/t4tEq'
    [lfs/t4tEq 5db746f] Automated test - lfs/t4tEq
    1 file changed, 3 insertions(+)
    create mode 100644 lfs/t4tEq
    Locking support detected on remote "origin". Consider enabling it with:
    $ git config lfs.http://localhost:3000/root/tp1.git/info/lfs.locksverify true
    Git LFS: (0 of 0 files, 1 skipped) 0 B / 0 B, 19 B skipped                                                         
       Counting objects: 4, done.
    Delta compression using up to 4 threads.
    Compressing objects: 100% (3/3), done.
    Writing objects: 100% (4/4), 516 bytes | 258.00 KiB/s, done.
    Total 4 (delta 0), reused 0 (delta 0)
    remote: 
    remote: To create a merge request for lfs/t4tEq, visit:
    remote:   http://localhost:3000/root/tp1/merge_requests/new?merge_request%5Bsource_branch%5D=lfs%2Ft4tEq
    remote: 
    To http://localhost:3000/root/tp1.git
    * [new branch]      lfs/t4tEq -> lfs/t4tEq
    Branch lfs/t4tEq set up to track remote branch lfs/t4tEq from origin.
    Switched to branch 'master'
    Your branch is up-to-date with 'origin/master'.
    ```
1. check the outcome
  1. Is the pipeline green?
  1. Can you download the LFS objects from the web interface?
  1. Can you dowload artifacts for the pipeline?
  1. Can you browse artifact archive for each job? (please check all the 3 jobs, they are different)
  1. Can you download a single file from the artifacts browsing of each job? (please check all the 3 jobs, they are different)
  1. Was `$du_enabled` respected? Check sidekiq logs: when `false` you should see a background move of LFS objects and artifacts
